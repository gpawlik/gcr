
# TODO: don't trigger validation while resolving, make it an optional, separate step
RSpec.describe GCR::Scheduler::RR do
  subject { described_class.new(workers) }

  let(:worker) do
    GCR::Worker.new(
      "worker",
      requires: [:foo],
      provides: [:bar],
    ) do |data|
      data[:bar] = "#{data.inspect}"
      data
    end
  end

  let(:another) do
    GCR::Worker.new(
      "worker",
      requires: [],
      provides: [:foo],
    ) do |data|
      data[:foo] = "#{data.inspect}"
      data
    end
  end

  let(:workers) { [worker, another] }

  it "returns last worker's result" do
    expect(subject.resolve).to eq(
      {
        foo: "#{{}.inspect}",
        bar: {foo: "#{{}.inspect}"}.inspect
      }
    )
  end
end
