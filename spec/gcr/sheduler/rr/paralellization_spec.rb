require 'benchmark'


RSpec.describe GCR::Scheduler::RR do
  subject { described_class.new(workers) }

  let(:worker) do
    GCR::Worker.new(
      "worker",
      requires: [],
      provides: [],
    ) do |_|
      sleep 2
      {}
    end
  end

  let(:another) do
    GCR::Worker.new(
      "worker",
      requires: [],
      provides: [],
    ) do |_|
      sleep 2
      {}
    end
  end

  let(:workers) do
    20.times.map do |i|
      GCR::Worker.new("worker_#{i}") do |_|
        sleep 0.3
        {}
      end
    end
  end

  it "returns last worker's result" do
    expect(Benchmark.measure { subject.resolve }.real).to be <= 1
  end
end
