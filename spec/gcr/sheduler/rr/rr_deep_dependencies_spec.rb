
RSpec.describe GCR::Scheduler::RR do
  subject { described_class.new(workers) }

  def nw(...)
    GCR::Worker.new(...)
  end

  let(:workers) do
    # This is the worst possible Fib implementation with extra dummy calculations
    # mul * 2 waits on mul
    # mul waits on numbers a1..a4
    # a4 waits on a3 and a2
    # a3 waists on a2 and a1
    # step_0 provides a1 and a2
    [
      nw("step_2", requires: %i[a2 a3], provides: %i[a4]) do |d|
        d[:a4] = d[:a2] + d[:a3]; d
      end,
      nw("step_1", requires: %i[a1 a2], provides: %i[a3]) do |d|
        d[:a3] = d[:a1] + d[:a2]; d
      end,
      nw("step_0", provides: %i[a1 a2]) do |_|
        {a1: 1, a2: 1}
      end,
      nw("multiply_all", requires: %i[a1 a2 a3 a4], provides: %i[mul]) do |d|
        d[:mul] = d[:a1] * d[:a2] * d[:a3] * d[:a4]; d
      end,
      nw("mul * 2", requires: %i[mul], provides: %i[mulX2]) do |d|
        d[:mulX2] = d[:mul] * 2; d
      end,
    ]
  end

  it "carries on calculations across deep depencencies" do
    expect(subject.resolve).to eq(
      {
        a1: 1,
        a2: 1,
        a3: 2,
        a4: 3,
        mul: 6,
        mulX2: 12
      }
    )
  end
end
