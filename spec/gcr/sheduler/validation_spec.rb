
# TODO: Replace RR with generic class, test only validation
# TODO: don't trigger validation while resolving, make it an optional, separate step
RSpec.describe GCR::Scheduler::RR do
  subject { described_class.new(workers) }

  let(:worker) do
    GCR::Worker.new(
      "worker",
      requires: [:foo],
      provides: [:bar],
    ) do |data|
      return data
    end
  end

  let(:workers) { [worker] }

  it do
    expect { subject.resolve }.to raise_error(/Unmet dependencies/)
  end

  context "circular dependencies" do
    let(:another) do
      GCR::Worker.new(
        "worker",
        requires: [:bar],
        provides: [:foo],
      ) do |data|
        return data
      end
    end

    let(:workers) { [worker] }

    xit do
      expect { subject.resolve }.to raise_error(/Circular dependencies/)
    end
  end
end
