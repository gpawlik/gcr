RSpec.describe GCR::AppendOnlyHash, aggregate_failures: true do
  subject { described_class.new }

  it { expect(subject.to_hash).to eql({}) }

  it do
    subject[:foo] = 42
    expect(subject.to_hash).to eq foo: 42
    expect(subject[:foo]).to eq 42
  end

  it do
    subject[:foo] = 42
    expect { subject[:foo] = 1 }.to raise_error /Immutable/
  end

  it do
    expect(subject.merge(foo: 42).to_hash).to eq foo: 42
    expect(subject.to_hash).to eq foo: 42

    expect { subject.merge(foo: 1) }.to raise_error /Immutable/
  end
end
