module GCR
  module Scheduler
    # Simples Round Robin Scheduler (just iterate over workers until they are all resolved)
    class RR
      # Workers?
      def initialize(workers)
        @workers = workers
      end

      def resolve
        diff = workers.flat_map(&:requires) - workers.flat_map(&:provides)
        raise "Unmet dependencies #{diff.inspect}" unless diff.empty?

        fibers = workers.map do |w|
          make_worker.tap { |f| f.resume(w.to_hash) }
        end

        data = {}
        while fibers.any?(&:alive?) do
          data = fibers
            .select(&:alive?)
            .map { |f| f.resume(data) }
            .select { |f| f.is_a? Thread }
            .reduce(data) { |mem, f| f.join.value }
        end
        data
      end

      private
      attr_reader :workers

      def make_worker
        Fiber.new do |data|
          definition = data.dup
          loop do
            if definition == data
              # First run
              data = {}
            else
              break if definition[:requires]
                .all? { |key| data.key?(key) }
            end
            data = Fiber.yield
          end
          Thread.new {
            definition[:block].call(data)
          }
        end
      end
    end
  end
end
