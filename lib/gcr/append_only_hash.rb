module GCR
  # TODO: add specs
  class AppendOnlyHash
    def initialize(h = Hash.new)
      @hash = h
    end

    def []=(key, value)
      raise "Immutable!" if @hash.key?(key)
      @hash[key] = value
    end

    def [](key)
      @hash.fetch(key)
    end

    def merge(h)
      h.each { |key, value| self[key] = value }
    end

    def to_hash
      @hash
    end
  end
end
