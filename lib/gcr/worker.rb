module GCR
  class Worker
    attr_reader :requires, :provides, :name, :block

    def initialize(name, requires: [], provides: [], &block)
      raise "Worker requires a block that performs work" unless block_given?

      @name = name
      @requires = requires
      @provides = provides
      @block = block
      @_assigned = {} # TODO: change to append only hash
    end

    def ready?(data)
      (requires - data.keys).empty?
    end

    def to_hash
      {name: -name, requires: requires.freeze, block: block}
    end

    def call(data)
      data
    end

    # Should a worker validate assigns, or scheduler?
  end
end
