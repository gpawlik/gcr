# frozen_string_literal: true

require_relative "gcr/version"
require_relative "gcr/worker"
require_relative "gcr/sheduler/rr"
require_relative "gcr/append_only_hash"

module GCR
  class Error < StandardError; end
  # Your code goes here...
end
